package com.team1.problem1;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;

public class JaccardCalculatorLauncher {
	
	private final String file;
	
	private final double jaccardCoefficient;
	
	private final boolean isLocal;
	
	public JaccardCalculatorLauncher(String file, double jaccardCoefficient, boolean isLocal) {
		this.file = file;
		this.jaccardCoefficient = jaccardCoefficient;
		this.isLocal = isLocal;
	}

	private void runAsDF(JaccardDataFrameCalculator jaccardCalculator) {
		SparkSession spark= 
				(isLocal)?
				SparkSession.builder()
				.appName("JaccardCalculator")
				.master("local")
				.getOrCreate():
				SparkSession.builder()
				.appName("JaccardCalculator")
				.getOrCreate();
		jaccardCalculator.process(spark, file);
		jaccardCalculator.print();
		spark.stop();
	}
	
	private void runAsRDD(JaccardRDDCalculator jaccardCalculator) {
		SparkConf conf = new SparkConf();
		
		conf.setAppName("JaccardCalculator")
			.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
			.set("spark.kryoserializer.buffer.max", "128m")
			.set("spark.kryoserializer.buffer", "64m")
			.set("spark.kryo.registrator", KryoSerializerRegistrator.RDDRegistrator.class.getName());
		if(isLocal)conf.setMaster("local");
		
		JavaSparkContext context = new JavaSparkContext(conf);
		
		jaccardCalculator.process(context, file);
		jaccardCalculator.print();
		context.close();
	}
	
	public void runAsInvertedDF() {
		runAsDF(new InvertedJaccardDataFrameCalculator(jaccardCoefficient));
	}
	
	public void runAsInvertedRDD() {
		runAsRDD(new InvertedJaccardRDDCalculator(jaccardCoefficient));
	}
	
	public void runAsInvertedRDD2() {
		runAsRDD(new InvertedJaccardRDDCalculator2(jaccardCoefficient));
	}
	
	public void runAsInvertedRDD3() {
		runAsRDD(new InvertedJaccardRDDCalculator3(jaccardCoefficient));
	}
	
	public void runAsListJoiningDF() {
		runAsDF(new BasicJaccardDataFrameCalculator(jaccardCoefficient));
	}
	
	public void runAsListJoiningRDD() {
		runAsRDD(new BasicJaccardRDDCalculator(jaccardCoefficient));
	}
	
	public void runAsMatrixRDD() {
		runAsRDD(new NaiveRDDBooleanArrayMatrixJaccardCalculator(jaccardCoefficient));
	}

}

