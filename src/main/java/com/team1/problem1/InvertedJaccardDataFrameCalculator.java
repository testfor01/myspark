package com.team1.problem1;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.collect_list;
import static org.apache.spark.sql.functions.expr;
import static org.apache.spark.sql.functions.sort_array;
import static org.apache.spark.sql.functions.split;

import java.util.List;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import scala.Serializable;

public class InvertedJaccardDataFrameCalculator implements JaccardDataFrameCalculator, Serializable {
	
	private final double jaccardConstant;
	private transient List<Row> result;
	
	public InvertedJaccardDataFrameCalculator(double jaccard) {
		this.jaccardConstant = jaccard / (jaccard + 1);
	}

	@Override
	public void process(SparkSession session, String file) {
		// TODO Auto-generated method stub
		Dataset<Row> rset = session.read().text(file);
		Dataset<Row> dset = rset
				.withColumn("v", split(col("value"),"\t").getItem(1).cast("int"))
				.withColumn("value", split(col("value"),"\t").getItem(0).cast("int"))
				.withColumnRenamed("value", "k");
		dset = dset
			.union(dset.select("v","k"))
			.groupBy("k")
			.agg(collect_list("v").alias("v"))
			.select(col("k"),sort_array(col("v")).alias("v"))
			.persist();
		
		result = dset.as("i")
			.join(dset.as("j"), col("i.k").cast("string").equalTo(col("j.k").cast("string")))
			.where(expr("i.v < j.v"))
			.withColumn("s", col("i.k").getItem(1).plus(col("j.k").getItem(1)))
			.groupBy("i.v","j.v","s")
			.count()
			.filter(col("count").$greater$eq(jaccardConstant * dset.select("v").where(expr("k==i.k or k==j.k")).groupBy("v").sum("v").first().getByte(0)))
			.collectAsList();
	}

	@Override
	public void print() {
		result.forEach(r->System.out.println(r.get(0) + "\t" + r.get(2)));
		System.out.println("total count : " + result.size());
	}
}
