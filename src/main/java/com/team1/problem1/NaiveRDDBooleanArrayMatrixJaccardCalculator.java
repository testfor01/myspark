package com.team1.problem1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;

import scala.Serializable;
import scala.Tuple2;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

@Deprecated
public class NaiveRDDBooleanArrayMatrixJaccardCalculator extends SymmetricJaccardMatrix implements JaccardRDDCalculator{

	private transient int[] vcount;
	
	private transient boolean[][] matrix;
	
	private int size;
	
	public NaiveRDDBooleanArrayMatrixJaccardCalculator(double jaccard) {
		super(jaccard);
	}
	
	@Override
	public void process(JavaSparkContext context, String file) {
		JavaRDD<String> stream = context.textFile(file);
		JavaPairRDD<Integer,Integer> streamPair;
		streamPair = stream.mapToPair(t-> {
				String[] s = t.split("\t");
				int i = Integer.parseInt(s[0]);
				int j = Integer.parseInt(s[1]);
				return new Tuple2<>(i,j);
			}
		);
		size = (int)streamPair.count();
		vcount = new int[size];
		matrix = new boolean[size][size];
		streamPair.foreach(x->{
			int i = x._1;
			int j = x._2;
			vcount[i] += 1;
			vcount[j] += 1;
			matrix[i][j] = true;
		});
		streamPair.flatMapToPair(t->
			new Iterator<Tuple2<Tuple2<Integer,Integer>,Integer>>(){
				int i = 0;
				@Override
				public boolean hasNext() {
					return i<size;
				}
				@Override
				public Tuple2<Tuple2<Integer, Integer>, Integer> next() {
					return new Tuple2<>(new Tuple2<>(t._1,t._2),getElement(t._1,i) & getElement(t._2,i++));
				}
		}
		).reduceByKey(
				(x,y)->x+y
		).foreach(x->matrix[x._1._2][x._1._1] = this.predict(matrix[x._1._1][x._1._2], getNumberOfElement(x._1._1) + getNumberOfElement(x._1._2), x._2));
	}

	public int getSize(JavaRDD<String> stream) {
		return stream.flatMap(x->Arrays.asList(x.split("\t")).iterator()).map(x->Integer.parseInt(x)).max(Comparator.naturalOrder()) + 1;
	}
	
	@Override
	public void print() {
		for(int i=0; i<size; i++)
			for(int j=i+1; j<size-1; j++) {
				if(matrix[j][i])
					System.out.println(i + "\t" + j);
			}
	}

	@Override
	protected int _getElementForLargerJ(int i, int j) {
		return matrix[i][j]?1:0;
	}

	@Override
	protected int _getSquareElementForLargerJ(int i, int j) {
		// TODO Auto-generated method stub
		return matrix[j][i]?1:0;
	}

	@Override
	protected int getNumberOfElement(int i) {
		// TODO Auto-generated method stub
		return vcount[i];
	}


}
