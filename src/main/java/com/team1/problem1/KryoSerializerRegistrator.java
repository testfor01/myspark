package com.team1.problem1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.spark.serializer.KryoRegistrator;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.serializers.CollectionSerializer;
import com.esotericsoftware.kryo.serializers.MapSerializer;
import com.esotericsoftware.kryo.serializers.FieldSerializer;

import scala.Tuple2;

public class KryoSerializerRegistrator {
	
	public static class RDDRegistrator implements KryoRegistrator {
        public void registerClasses(Kryo kryo) {
          kryo.register(LinkedList.class, new CollectionSerializer());
          kryo.register(ArrayList.class, new CollectionSerializer());
          kryo.register(Map.class, new MapSerializer());
          kryo.register(Tuple2.class, new FieldSerializer<Object>(kryo, Tuple2.class));
        }
    }
	
	
	

}
