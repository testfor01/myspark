package com.team1.problem1;

import scala.Serializable;

public abstract class SymmetricJaccardMatrix implements Matrix<Integer>, JaccardPredictable, Serializable{
	
	private double jaccard;
	
	protected SymmetricJaccardMatrix(double jaccard) {
		this.jaccard = jaccard / (jaccard+1);
	}

	@Override
	public Integer getSquaredElement(int i, int j) {
		return (i<j)?_getSquareElementForLargerJ(j,i):_getSquareElementForLargerJ(i,j);
	}

	@Override
	public Integer getElement(int i, int j) {
		return (i<j)?_getElementForLargerJ(j,i):_getElementForLargerJ(i,j);
	}

	protected abstract int _getElementForLargerJ(int i, int j);
	
	protected abstract int _getSquareElementForLargerJ(int i, int j);
	
	protected abstract int getNumberOfElement(int i);
	
	@Override
	public double getJaccard() {
		return jaccard;
	}
	

}
