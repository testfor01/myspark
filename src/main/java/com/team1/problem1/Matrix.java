package com.team1.problem1;


public interface Matrix<E> {
	
	/**
	 * 
	 * @param i
	 * @param j
	 * @return M(i,j)
	 */
	public E getElement(int i, int j);
	
	/**
	 * 
	 * @param i
	 * @param j
	 * @return M^2(i,j)
	 */
	public E getSquaredElement(int i, int j);

	

}
