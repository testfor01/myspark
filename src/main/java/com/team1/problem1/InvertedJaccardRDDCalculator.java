package com.team1.problem1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.HashPartitioner;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;

import scala.Serializable;
import scala.Tuple2;
import com.team1.problem1.LinkedList;
import com.team1.problem1.LinkedList.Node;

public class InvertedJaccardRDDCalculator implements JaccardRDDCalculator, JaccardPredictable, Serializable{

	private static final long serialVersionUID = 5577404374824941678L;
	
	private final double jaccard;
	
	private transient List<Tuple2<Integer, Integer>> result;
	
	public InvertedJaccardRDDCalculator(double jaccard) {
		this.jaccard = jaccard / (jaccard + 1);
	}
	
	@Override
	public double getJaccard() {
		return this.jaccard;
	}

	@Override
	public void process(JavaSparkContext context, String file) {
		JavaRDD<String> stream = context.textFile(file);
		JavaPairRDD<Integer,LinkedList<Integer>> invertedKVStream = stream
			.flatMapToPair(x->
					{
						String[] s = x.split("\t"); // x를 탭 기호 기준으로 자른다.
						int i = Integer.parseInt(s[0]); // 앞에것을 i로
						int j = Integer.parseInt(s[1]); // 뒤에것을 j로 넣는다.
						LinkedList<Integer> li = new LinkedList<Integer>();
						LinkedList<Integer> lj = new LinkedList<Integer>();
						li.add(i);
						lj.add(j);
						return Arrays.asList( // key:value를 나눈다, (i,j) (j,i) 두가지 형태를 즉시 생성해서 map에 결과값에 넣는다. 
												//value는 O(1) 시간내 append할 수 있도록 LinkedList형으로 구현한다.
								new Tuple2<>(i,lj),
								new Tuple2<>(j,li)
						).iterator();
					}
				)
			// reduce로 구현시, 정렬된 2개의 value 블록을 merge하는 방법을 통해 reduce한다. 
			// 이는 Θ(nlog(n))의 시간을 요구한다.
			// groupby를 사용할 시, iterative하게 블록을 하나씩 읽어서 sequence에 채워넣으므로 Θ(n^2)의 시간이 든다.
			.reduceByKey((x,y)->x.concat(y));
		
		//map을 생성한다. map은 차후 RDD에서 호출시 immutable한 dictionary이 된다.
		Map<Integer,LinkedList<Integer>> dictRDD = new HashMap<>();
		dictRDD.putAll(invertedKVStream.collectAsMap());
		Broadcast<Map<Integer,LinkedList<Integer>>> dictBroadcast = context.broadcast(dictRDD);
		result = invertedKVStream.flatMapToPair(x->
			{
				int size = x._2.size();
				List<Tuple2<Tuple2<Integer,Integer>,Integer>> list = new ArrayList<Tuple2<Tuple2<Integer,Integer>,Integer>>(size);
				Node<Integer> target = x._2.first ,target2;

				if(target != null) {
					for(;target.next!=null; target2 = (target = target.next)) {
						for(target2 = target.next; target2 !=null; target2 = target2.next)
							if(target.item < target2.item)
								list.add(new Tuple2<>(new Tuple2<>(target.item,target2.item),1));
							else
								list.add(new Tuple2<>(new Tuple2<>(target2.item,target.item),1));
					}
				}
				return list.iterator();
			}
		)
		.reduceByKey((x,y)->x+y)
		.filter(x->{
			Map<Integer,LinkedList<Integer>> dict = dictBroadcast.value();
			LinkedList<Integer> list = dict.get(x._1._1);
			int target = x._1._2;
			boolean isConnected = false;
			for(int i:list) {
				if(i==target) {
					isConnected = true;
				}
				if(i>=target)
					break;
			}
			return predict(isConnected, list.size()+dict.get(target).size(), x._2);
		})
		.sortByKey(new TupleComparator())
		.keys()
		.collect();
	}

	@Override
	public void print() {
		result.forEach(x->System.out.println(x._1 + "\t" + x._2));
		System.out.println("total count : " + result.size());
	}
	

} 
