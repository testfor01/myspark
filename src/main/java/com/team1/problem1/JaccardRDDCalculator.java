package com.team1.problem1;

import org.apache.spark.api.java.JavaSparkContext;

public interface JaccardRDDCalculator {
	
	/**
	 * @param context JavaSparkContext
	 * @param file edge file path
	 * make result before print() invoked
	 */
	public void process(JavaSparkContext context, String file);
	
	/**
	 * print all line of result
	 */
	public void print();

}
