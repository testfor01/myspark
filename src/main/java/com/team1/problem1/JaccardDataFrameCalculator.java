package com.team1.problem1;

import org.apache.spark.sql.SparkSession;

public interface JaccardDataFrameCalculator {
	
	/**
	 * 
	 * @param session
	 * @param file
	 */
	public void process(SparkSession session, String file);
	
	/**
	 * print all line of result
	 */
	public void print();

}
