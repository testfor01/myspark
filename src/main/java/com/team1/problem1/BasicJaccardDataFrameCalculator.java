package com.team1.problem1;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.api.java.UDF2;
import org.apache.spark.sql.types.DataTypes;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.split;

import java.util.List;

import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.mutable.WrappedArray;

import static org.apache.spark.sql.functions.expr;
import static org.apache.spark.sql.functions.not;
import static org.apache.spark.sql.functions.asc;
import static org.apache.spark.sql.functions.collect_set;
import static org.apache.spark.sql.functions.collect_list;
import static org.apache.spark.sql.functions.sort_array;

import scala.Serializable;

public class BasicJaccardDataFrameCalculator implements JaccardDataFrameCalculator, Serializable{

	private final double jaccardConstant;
	
	private transient List<Row> result;
	
	public BasicJaccardDataFrameCalculator(double jaccard) {
		this.jaccardConstant = jaccard / (jaccard + 1);
	}

	@Override
	public void process(SparkSession session, String file) {
		// TODO Auto-generated method stub
		Dataset<Row> rset = session.read().text(file);
		session.udf().register("array_intersect_size",
				  new UDF2<WrappedArray<Integer>,WrappedArray<Integer>,Integer>(){
						@Override
						public Integer call(WrappedArray<Integer> t1, WrappedArray<Integer> t2) throws Exception {
							Iterator<Integer> a = t1.iterator();
							Iterator<Integer> b = t2.iterator();
							int intersection = 0;
							int p = a.next();
							int q = b.next();
							
							while(a.hasNext() && b.hasNext()){
								if(p<q) {
									p = a.next();
								}else if(p>q) {
									q = b.next();
								}else {
									p = a.next();
									q = b.next();
									intersection++;
								}
							}
							while(a.hasNext() && p < q)
								p = a.next();
							while(b.hasNext() && q< p)
								q = b.next();
							if(p==q)intersection++;
							return intersection;
						}
					},DataTypes.IntegerType
		);
		session.udf().register("binary_search",
				  new UDF2<WrappedArray<Integer>,Integer,Boolean>(){
						@Override
						public Boolean call(WrappedArray<Integer> t, Integer e) throws Exception {
							int l = 0, r = t.size()-1, m, result; 
					        while (l <= r) { 
					        	m = (r+l)/2;
					        	result = t.apply(m);
					        	if(result == e) 
					        		return true;
					        	if(result < e) 
					        		l = m + 1;
					        	else
					        		r = m - 1; 
					        }return false; 
						}
					},DataTypes.BooleanType
		);
		Dataset<Row> dset = rset
				.withColumn("v", split(col("value"),"\t").getItem(1).cast("int"))
				.withColumn("value", split(col("value"),"\t").getItem(0).cast("int"))
				.withColumnRenamed("value", "k");
		dset = dset
			.union(dset.select("v","k"))
			.groupBy("k")
			.agg(collect_list("v").alias("v"))
			.select(col("k"),sort_array(col("v")).alias("v"));
		
		result = dset.as("i")
				.join(
					dset.as("j"),
						expr("i.k<j.k").and(
						not(expr("array_contains(i.v,j.k)"))).and(
						expr("array_intersect_size(i.v,j.v) > (" + jaccardConstant + ")*(size(i.v)+size(j.v))"))
				)
				.orderBy(asc("i.k"),asc("j.k"))
				.select("i.k","j.k")
				.collectAsList();
	}

	@Override
	public void print() {
		result.forEach(t->System.out.println(t.get(0) + "\t" + t.get(1)));
		System.out.println("total count : " + result.size());
	}

}
