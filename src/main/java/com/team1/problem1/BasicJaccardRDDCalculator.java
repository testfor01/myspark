package com.team1.problem1;

import java.util.Iterator;
import java.util.List;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import scala.Serializable;
import scala.Tuple2;
import scala.Tuple3;

public class BasicJaccardRDDCalculator implements JaccardRDDCalculator, JaccardPredictable, Serializable {
	
	private transient List<Tuple2<Tuple2<Integer,Iterable<Integer>>, Tuple2<Integer,Iterable<Integer>>>> result;
	
	private final double jaccard;
	
	public BasicJaccardRDDCalculator(double jaccard) {
		this.jaccard = jaccard / (jaccard+1);
	}

	@Override
	public void process(JavaSparkContext context, String file) {
		// TODO Auto-generated method stub
		JavaRDD<String> first = context.textFile(file);
		JavaPairRDD<Integer, Iterable<Integer>> second = toGroup(first);
		JavaPairRDD<Tuple2<Integer,Iterable<Integer>>, Tuple2<Integer,Iterable<Integer>>> third = toJoin(second);
		result = third.collect();
	}
	
	private JavaPairRDD<Integer, Iterable<Integer>> toGroup(JavaRDD<String> stream){
		return stream.mapToPair(x->{
			String[] s = x.split("\t", 2);
			return new Tuple2<>(Integer.parseInt(s[0]),Integer.parseInt(s[1]));
		})
		.groupByKey()
		.sortByKey();
	}
	
	private JavaPairRDD<Tuple2<Integer,Iterable<Integer>>, Tuple2<Integer,Iterable<Integer>>> toJoin(JavaPairRDD<Integer, Iterable<Integer>> stream){
		return stream.cartesian(stream).filter(
				x-> {
					if(x._1._1 >= x._2._1)
						return false;
					else {
						Tuple3<Integer,Integer, Boolean> source = getSumAndIntersectionAndOverlap(x._2._1,x._1._2.iterator(),x._2._2.iterator());
						return predict(
								source._3(), // {i,j} == true?
								source._1(), //|i|+|j|
								source._2()  //|i n j|
						);
					}				
			}
		);
	}
	
	public Tuple3<Integer,Integer, Boolean> getSumAndIntersectionAndOverlap(Integer right, Iterator<Integer> a, Iterator<Integer> b){
		boolean overlap = false;
		int p = 0,q = 0;
		int sum = 0;
		int intersection = 0;
		while(a.hasNext() && b.hasNext()) {
			if(p<q) {
				p = a.next();
				if(right == p)
					overlap = true;
				sum++;
			}else if(p>q) {
				q = b.next();
				sum++;
			}else {
				p = a.next();
				if(right == p)
					overlap = true;
				q = b.next();
				intersection++;
				sum+=2;
			}
		}
		while(a.hasNext()) {
			a.next();
			sum++;
		}
		while(b.hasNext()) {
			b.next();
			sum++;
		}
		return new Tuple3<Integer,Integer,Boolean>(sum,intersection,overlap);
	}

	@Override
	public void print() {
		for(Tuple2<Tuple2<Integer,Iterable<Integer>>, Tuple2<Integer,Iterable<Integer>>> t : result) {
			System.out.println(t._1._1 + "\t" + t._2._1);
		}
	}


	@Override
	public double getJaccard() {
		return jaccard;
	}

}
