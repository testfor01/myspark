package com.team1.problem1;

public interface JaccardPredictable {
	
	/**
	 * @return preset jaccard constant = jaccardThreshold/(jaccardThreshold+1)
	 */
	public double getJaccard();
	
	/**
	 * 
	 * @param itoj
	 * @param sumOfSize
	 * @param overlap
	 * @return check if it is ok to introduce i'th node to j'th node
	 */
	public default boolean predict(boolean itoj, int sumOfSize, int overlap) {
		return 
				(!itoj) && 
				(overlap >= getJaccard() * sumOfSize);
	}

}
