package com.team1.problem1;

import java.io.Serializable;
import java.util.Comparator;

import scala.Tuple2;

public class TupleComparator implements Serializable,Comparator<Tuple2<Integer, Integer>>{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5337721260198639921L;

	@Override
    public int compare(Tuple2<Integer, Integer> o1, Tuple2<Integer, Integer> o2) {
        return (o1._1==o2._1)?o1._2-o2._2:o1._1-o2._1;
    }
	
}
