package com.team1.problem1;

import java.io.IOException;

public class Main {
	public static void main(String[] args) throws IOException {

		JaccardCalculatorLauncher jcl = new JaccardCalculatorLauncher(args[0],Double.parseDouble(args[1]), false);
		jcl.runAsInvertedRDD();
	}
}

