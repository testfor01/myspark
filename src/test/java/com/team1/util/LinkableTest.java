package com.team1.util;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import com.team1.problem1.LinkedList;

import scala.collection.mutable.WrappedArray;



public class LinkableTest {
	LinkedList<Integer> list1;
	LinkedList<Integer> list2;

	@Before
	public void setUp() throws Exception {
		list1 = new LinkedList<Integer>();
		list2 = new LinkedList<Integer>();
		list1.addAll(Arrays.asList(1,2,3,4));
		list2.addAll(Arrays.asList(5,6,7,9,15));
	}

	@Test
	public void test() {	
		assertEquals(list1.reverseConcat(list2),new LinkedList<Integer>(Arrays.asList(5,6,7,9,15,1,2,3,4)));
		assertEquals(list1.size(),9);
		list2 = list1.nextLinkedList();
		assertArrayEquals(list2.toArray(),(new LinkedList<Integer>(Arrays.asList(6,7,9,15,1,2,3,4)).toArray()));
		assertEquals(list2.size(),8);
	}

}
